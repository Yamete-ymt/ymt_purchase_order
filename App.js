import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, TextInput} from 'react-native';

class App extends Component{

  state = {
    page : "main",
    title : "",
    description:"",
    quantity:"",
    items : [
      {title: "apple", description: "can be eaten", quantity: "100" },
      {title: "orange", description: "got from mars", quantity: "2"},
      {title: "watermalon", description: "it tastes spicy", quantity: "50"},
    ]
  }
  formpage = () =>{this.setState({page:"form"})}
  main = () =>{this.setState({page:"main"})}
  add = () => {
    let title = this.state.title;
    let description = this.state.description;
    let quantity = this.state.quantity;

    this.setState(
      {
        page : "main",
        items : [
          ...this.state.items,
          {title: title, description: description ,quantity : quantity}
        ]
      }
    )
  }
  render(){
    if (this.state.page=="main") {
      return(
        <ScrollView style={styles.container}>
          <View style={styles.head}>
            <Text style={styles.header}>Items list</Text>
            <TouchableOpacity style={styles.add} onPress={this.formpage}>
              <Text style={styles.circle}>+</Text>
            </TouchableOpacity>
          </View>
          <View>
            <Text>{'\n'}</Text>
          </View>
  
          {this.state.items.map( i => {
            return(
              <View>
                <View style={styles.listing}>
                  <Text style={{fontWeight:"bold"}}>Title : </Text>
                  <Text>{i.title}</Text>
                </View>
                <View style={styles.listing}>
                  <Text style={{fontWeight:"bold"}}>Description : </Text>
                  <Text>{i.description}</Text>
                </View>
                <View style={styles.listing}>
                  <Text style={{fontWeight:"bold"}}>Quantity : </Text>
                  <Text>{i.quantity}</Text>
                </View>
                <Text>{'\n'}</Text>
              </View>
            )
          })}
       </ScrollView>
      )
    }
    if (this.state.page=="form") {
      return(
        <ScrollView style={styles.container}>
          <View style={styles.head}>
            <Text style={styles.header}>Add items</Text>
          </View>
          <View>
              <TextInput style={styles.input} onChangeText={(text)=>{this.setState({title:text})}} placeholder="Item Title"/>
              <TextInput style={styles.input} onChangeText={(text)=>{this.setState({description:text})}} placeholder="Description"/>
              <TextInput style={styles.input} onChangeText={(text)=>{this.setState({quantity:text})}} placeholder="Quantity"/>
              <Text>{'\n'}</Text>
          </View>
          <View style={styles.buttonsview}>
            <TouchableOpacity style={styles.addbutton}  onPress={this.add}>
              <Text style={styles.buttontext}>Add</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.backbutton} onPress={this.main}>
              <Text style={styles.buttontext}>Back</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      )
    }
  }
}
   
const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "#E0F8E6",
  },
  head:{
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 17,
  },
  add:{
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 50,
    marginRight: 17,
    backgroundColor: "green"
  },
  circle:{
    fontSize: 40,
    color: "white",
  },
  header:{
    fontSize: 40,
    fontFamily: "serif",
    fontWeight: "bold",
    color: "#2E2E2E",
    padding: 10,
  },
  listing:{
    flex: 1,
    flexDirection: "row",
    paddingLeft: 10,
  },
  buttonsview:{
    flex:1,
    flexDirection:"row",
  },
  addbutton:{
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: "center",
    padding: 10,
    width: "50%",
    height: 50,
    backgroundColor:"#0B610B",
  },
  backbutton:{
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: "center",
    padding: 10,
    width: "50%",
    height: 50,
    backgroundColor:"#FE2E2E",
  },
  buttontext:{
    color:"white",
  },
  input:{
    borderWidth: 1,
    borderColor: "black",
    width: "100%",
    height: 50,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  }
})
export default App;